## full_k61v1_64_bsp-user 9 PPR1.180610.011 1586831913 release-keys
- Manufacturer: ulefone
- Platform: mt6761
- Codename: Note_7P
- Brand: Ulefone
- Flavor: full_k61v1_64_bsp-user
- Release Version: 9
- Id: PPR1.180610.011
- Incremental: 1586831913
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Note_7P/Note_7P:9/PPR1.180610.011/1586831913:user/release-keys
- OTA version: 
- Branch: full_k61v1_64_bsp-user-9-PPR1.180610.011-1586831913-release-keys
- Repo: ulefone_note_7p_dump_559


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
