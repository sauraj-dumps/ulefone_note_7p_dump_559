#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:ee8c5c3b521705c6637d8f8ce2a4496933ab1b8e; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:b7f15400d07c2c212529e3b6bebf2b065fc8529a EMMC:/dev/block/platform/bootdevice/by-name/recovery ee8c5c3b521705c6637d8f8ce2a4496933ab1b8e 33554432 b7f15400d07c2c212529e3b6bebf2b065fc8529a:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
